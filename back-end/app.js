const bodyParser = require('body-parser');
const express = require('express');
const cors = require('cors');

const app = express();

let nextId = 1;

app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const PORT = 3000;

const dados = [];

app.get('/', (req, res) => {
  res.send('Servidor funcionando');
});

app.post('/Cadastro', (req, res) => {
  const newMember = req.body;
  newMember.id = nextId++; // Atribui o próximo ID e, em seguida, o incrementa
  dados.push(newMember);
  res.send(dados);
});

// Rota para excluir um elemento com base no ID
app.delete('/Cadastro/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  const index = dados.findIndex((item) => item.id === id);

  if (index !== -1) {
    dados.splice(index, 1);
    res.send({ success: true, message: 'Elemento excluído com sucesso.' });
  } else {
    res.status(404).send({ success: false, message: 'Elemento não encontrado.' });
  }
});

// Rota para atualizar um elemento com base no ID
app.put('/Cadastro/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  const updatedData = req.body;

  const index = dados.findIndex((item) => item.id === id);

  if (index !== -1) {
    // Se o elemento com o ID for encontrado, atualize-o
    dados[index] = { ...dados[index], ...updatedData };
    res.send({ success: true, message: 'Elemento atualizado com sucesso.' });
  } else {
    // Se o elemento com o ID não for encontrado, envie uma mensagem de erro
    res.status(404).send({ success: false, message: 'Elemento não encontrado.' });
  }
});

app.listen(PORT, () => {
  console.log('Servidor funcionando', PORT);
});
