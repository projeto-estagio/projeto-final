
const { createApp } = require('vue');

members = [
    {
        id: '',
        fname: '',
        tipo: '',
        massa: '',
        volum: ''
    }
    
]

const handlingForms = {

    data() {
        return {
            members: window.members,
            newMember: {

            }
        }
    },
    

    methods: {
        addMember: function() {
            if (!(this.newMember.id && this.newMember.fname && this.newMember.massa && this.newMember.tipo&& this.newMember.volum)) {
                alert("Todos os campos precisam ser preenchidos!")
            } else {
                this.members.push(this.newMember);
                this.newMember = {};
            }
        }
    }

};

let totalMassa = 0;

for (const objeto of handlingForms) {
    totalMassa += objeto.massa;
}
document.getElementById("totalMassa").textContent = totalMassa;
const vue = createApp(handlingForms);
vue.mount('#app')
